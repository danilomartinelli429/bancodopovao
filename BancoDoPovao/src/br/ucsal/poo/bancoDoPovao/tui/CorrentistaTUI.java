package br.ucsal.poo.bancoDoPovao.tui;

import java.util.List;
import java.util.Scanner;

import br.ucsal.poo.bancoDoPovao.business.CorrentistaBO;
import br.ucsal.poo.bancoDoPovao.domain.Correntista;

public class CorrentistaTUI {
	public Scanner scanner = new Scanner(System.in);
	public CorrentistaBO CorrentistaBO = new CorrentistaBO();

	public void incluir() {
		System.out.println("############ INCLUIR CORRENTISTA ############");
		String nome = obterTexto("Informe o nome:");
		String cpf = obterTexto("Infome o cpf:");
		String telefone = obterTexto("Infome o telefone:");
		String endereco = obterTexto("Informe o endereço:");
		Double rendaMensalMedia = obterDouble("Informe a renda mensal média:");
		String rg = obterTexto("Informe o rg:");
		
		Correntista correntista = new Correntista(nome, cpf, telefone, endereco, rendaMensalMedia, rg, false, false);
		
		try {
			CorrentistaBO.criarNovoCorrentista(correntista);
		} catch (IllegalArgumentException err) {
			System.out.println(err.getMessage());
		} catch (Exception err) {
			System.out.println("Um erro aconteceu: " + err.getMessage());
		}
	}
	
	public void listarTodos() {
		List<Correntista> correntistas = CorrentistaBO.obterTodosCorrentistas();
		
		if (correntistas.isEmpty()) {
			System.out.println("Nenhum correntista cadastrado");
		} else {
			for (Correntista correntista : correntistas) {
				System.out.println(correntista.toString());
			}
		}
	}

	private String obterTexto(String mensagem) {
		System.out.println(mensagem);
		return scanner.nextLine();
	}

	private Double obterDouble(String mensagem) {
		System.out.println(mensagem);
		Double numero = scanner.nextDouble();
		scanner.nextLine();
		return numero;
	}

}
