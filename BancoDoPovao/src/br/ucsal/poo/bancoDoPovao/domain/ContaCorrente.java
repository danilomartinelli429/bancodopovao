package br.ucsal.poo.bancoDoPovao.domain;

import br.ucsal.poo.bancoDoPovao.enums.StatusContaCorrente;

public class ContaCorrente {
	private static Integer totalContas = 0;
	
	private Integer numeroConta;
	private Double saldo = 0d;
	private Double limiteCredito = 0d;
	private StatusContaCorrente status = StatusContaCorrente.ATIVA;
	
	public ContaCorrente() {
		totalContas += 1;
		this.numeroConta = totalContas;
	}
	
	public Integer getNumeroConta() {
		return numeroConta;
	}

	public Double getLimiteCredito() {
		return limiteCredito;
	}

	public void setLimiteCredito(Double limiteCredito) {
		this.limiteCredito = limiteCredito;
	}
	
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	
	public Double getSaldo() {
		return saldo;
	}

	public StatusContaCorrente getStatus() {
		return status;
	}

	public void setStatus(StatusContaCorrente status) {
		this.status = status;
	}
}
