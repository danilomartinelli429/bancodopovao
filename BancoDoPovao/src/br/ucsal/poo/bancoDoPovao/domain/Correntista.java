package br.ucsal.poo.bancoDoPovao.domain;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

public class Correntista implements Serializable {
	private static final long serialVersionUID = 2L;
	
	private String nome;
	private String cpf;
	private String telefone;
	private String endereco;
	private Double rendaMensalMedia;
	private String rg;
	private Boolean comprovanteResidencia;
	private Boolean comprovanteRenda;
	private List<ContaCorrente> contasCorrentes = new ArrayList<>();
	
	public Correntista(String nome, String cpf, String telefone, String endereco, Double rendaMensalMedia, String rg, Boolean comprovanteResidencia, Boolean comprovanteRenda) {
		this.nome = nome;
		this.cpf = cpf;
		this.telefone = telefone;
		this.endereco = endereco;
		this.rendaMensalMedia = rendaMensalMedia;
		this.rg = rg;
		this.comprovanteResidencia = comprovanteResidencia;
		this.comprovanteRenda = comprovanteRenda;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Double getRendaMensalMedia() {
		return rendaMensalMedia;
	}

	public void setRendaMensalMedia(Double rendaMensalMedia) {
		this.rendaMensalMedia = rendaMensalMedia;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public Boolean getComprovanteResidencia() {
		return comprovanteResidencia;
	}

	public void setComprovanteResidencia(Boolean comprovanteResidencia) {
		this.comprovanteResidencia = comprovanteResidencia;
	}

	public Boolean getComprovanteRenda() {
		return comprovanteRenda;
	}

	public void setComprovanteRenda(Boolean comprovanteRenda) {
		this.comprovanteRenda = comprovanteRenda;
	}

	public List<ContaCorrente> getContasCorrentes() {
		return contasCorrentes;
	}

	public void setContasCorrentes(List<ContaCorrente> contasCorrentes) {
		this.contasCorrentes = contasCorrentes;
	}

	@Override
	public String toString() {
		return "Correntista [nome=" + nome + ", cpf=" + cpf + ", telefone=" + telefone + ", endereco=" + endereco
				+ ", rendaMensalMedia=" + rendaMensalMedia + ", rg=" + rg + ", comprovanteResidencia="
				+ comprovanteResidencia + ", comprovanteRenda=" + comprovanteRenda + ", contasCorrentes="
				+ contasCorrentes + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comprovanteRenda == null) ? 0 : comprovanteRenda.hashCode());
		result = prime * result + ((comprovanteResidencia == null) ? 0 : comprovanteResidencia.hashCode());
		result = prime * result + ((contasCorrentes == null) ? 0 : contasCorrentes.hashCode());
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((rendaMensalMedia == null) ? 0 : rendaMensalMedia.hashCode());
		result = prime * result + ((rg == null) ? 0 : rg.hashCode());
		result = prime * result + ((telefone == null) ? 0 : telefone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Correntista other = (Correntista) obj;
		
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		return true;
	}
	
	
	
}
