package br.ucsal.poo.bancoDoPovao.persistence;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.poo.bancoDoPovao.domain.Correntista;

public class CorrentistaDAO {
	private static final String NOME_ARQUIVO = "correntistas.dat";

	public List<Correntista> correntistas = new ArrayList<>();

	public CorrentistaDAO() {
		lerCorrentistasDisco();
	}

	public void incluir(Correntista correntista) {
		correntistas.add(correntista);
		gravarCorrentistasDisco();
	}
	
	public void incluirContaCorrente(Correntista correntista) {
		List<Correntista> correntistas = obterTodos();
		
		for (int index = 0; index < correntistas.size(); index++) {
			if (correntistas.get(index).equals(correntista)) {
				correntistas.get(index).setContasCorrentes(correntista.getContasCorrentes());
				break;
			}
		}
	}

	private void gravarCorrentistasDisco() {
		try {
			ObjectOutput objectOutput = new ObjectOutputStream(new FileOutputStream(NOME_ARQUIVO));
			objectOutput.writeObject(correntistas);
			objectOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void lerCorrentistasDisco() {
		try {
			ObjectInput objectInput = new ObjectInputStream(new FileInputStream(NOME_ARQUIVO));
			correntistas = (List<Correntista>) objectInput.readObject();
			objectInput.close();
		} catch (EOFException e) {
			correntistas.clear();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public List<Correntista> obterTodos() {
		return correntistas;
	}

}
