package br.ucsal.poo.bancoDoPovao.business;

import java.util.List;

import br.ucsal.poo.bancoDoPovao.domain.ContaCorrente;
import br.ucsal.poo.bancoDoPovao.domain.Correntista;
import br.ucsal.poo.bancoDoPovao.persistence.CorrentistaDAO;
import br.ucsal.poo.bancoDoPovao.utils.CPF;

public class CorrentistaBO {
	private CorrentistaDAO correntistaDAO = new CorrentistaDAO();
	
	public void criarNovoCorrentista(Correntista correntista) {
		try {
			validarNovoCorrentista(correntista);
			correntistaDAO.incluir(correntista);
		} catch (IllegalArgumentException err) {
			throw new IllegalArgumentException(err.getMessage());
		}
	}
	
	private void validarNovoCorrentista(Correntista correntista) {
		if (correntista.getNome().trim().isEmpty()) {
			throw new IllegalArgumentException("Campo nome não pode ser vazio");
		} else if (!(new CPF(correntista.getCpf()).validarCPF())) {
			throw new IllegalArgumentException("Campo CPF não válido");
		} else if (correntista.getTelefone().trim().isEmpty()) {
			throw new IllegalArgumentException("Campo telefone não pode ser vazio");
		} else if (correntista.getEndereco().trim().isEmpty()) {
			throw new IllegalArgumentException("Campo telefone não pode ser vazio");
		} else if (correntista.getRendaMensalMedia() <= 0) {
			throw new IllegalArgumentException("Campo renda mensal média não válido");
		} else if  (correntista.getRg().trim().isEmpty()) {
			throw new IllegalArgumentException("Campo rg não pode ser vazio");
		}
		
		List<Correntista> correntistas = correntistaDAO.obterTodos();
		for (Correntista e : correntistas) {
			System.out.println(e.getCpf());
			System.out.println(correntista.getCpf());
			if (e.getCpf().equals(correntista.getCpf())) {
				throw new IllegalArgumentException("CPF já cadastrado");
			}
		}
	}
	
	public List<Correntista> obterTodosCorrentistas() {
		return correntistaDAO.obterTodos();
	}
	
	public void criarContaCorrente(Correntista correntista) {
		ContaCorrente contaCorrente = new ContaCorrente();
		List<ContaCorrente> contasCorrentes = correntista.getContasCorrentes();
		
		contasCorrentes.add(contaCorrente);
		correntista.setContasCorrentes(contasCorrentes);
		
		correntistaDAO.incluirContaCorrente(correntista);
		
	}
}
