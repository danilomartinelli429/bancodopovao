package br.ucsal.poo.bancoDoPovao.enums;

public enum StatusContaCorrente {
	ATIVA(1), BLOQUEADA(2), ENCERRADA(3);
    
    private final int valor;
    
    StatusContaCorrente(int valor){
        this.valor = valor;
    }
    
    public int getValor(){
        return valor;
    }
}
